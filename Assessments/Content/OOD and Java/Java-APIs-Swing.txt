
==========
Objective: Given a scenario, select an appropriate layout manager
==========


[[
You need to build a dialog wizard that has five panels that are
navigated one at a time using Next and Back buttons.

Which layout manager would you use in order to show one panel and hide the others?
]]
1: <code>BoxLayout</code>
*2: <code>CardLayout</code>
3: <code>GridLayout</code>
4: <code>GridBagLayout</code>


==========
Objective: Describe the rules for threading in a Swing application
==========

[[
Given a Swing application that is launched from the MyApp class:
<pre>
1. public class MyApp {
2.    public static void main(String[] args) {
3.       // ADD CODE HERE
4.    }
5. }
</pre>

and the application's main window frame class:
<pre>
public class MainFrame extends JFrame {
   public MyFrame() {
      // initialize View (not shown)
      setVisible(true);  // show the main frame
   }
   // more code here
}
</pre>

Which code snippet on line 3 of <code>MyApp</code> is the recommended way of
launching the application's main frame?
]]
1: <code>new MainFrame();</code>
2: <code>EventQueue.launchApp(new MainFrame());</code>
3: <code>SwingUtilities.invokeLater(new MainFrame());</code>
4: <code>EventQueue.invokeOnThread(() -> { new MainFrame(); });</code>
*5: <code>SwingUtilities.invokeLater(() -> { new MainFrame(); });</code>
[[NOTE: team decided this technique is niche knowledge.]]

[[
You are building a Swing application that requires invoke a remote service
when a specific button is pushed.  Here is the event handler code:

<pre>button.addActionListener((event) ->
   List<Customer> customers = myService.queryCustomers(/* query params */);
   /* update a GUI table with these customers */
);</pre>

In some scenarios, this query can take up to a minute to execute.  Several
users have complained that the UI freezes up when the button if clicked during
this query.

Which two techniques resolve the perceived response issue and adhere to Swing's
threading guidelines?  (Choose two)
]]
1: Wrap all of the event handler code in a <code>Runnable</code> class and then use
<code>SwingUtilities.invokeLater</code> method on that runnable within the action listener.

*2: Create a <code>SwingWorker</code> implementation that puts the query code in the
<code>doInBackground</code> method and the table refresh code in the <code>done</code> method.
The action listener then creates an instance of your query worker and calls the <code>execute</code> method.

3: Wrap the query code in a <code>Runnable</code> class and pass an instance of this object to
the <code>SwingUtilities.invokeAndWait</code> method in the action listener.  Swing will invoke
the query runnable in a new thread; the action listener thread then waits until that is done.
When done the GUI table update code is then run in the Event Queue as intended.

*4: Wrap the query code in a <code>Runnable</code> class and launch that runnable object
is a separate thread.  Start that thread in the action listener.  Create another <code>Runnable</code>
class that contains the GUI table update code.  Make an instance of that class in the first
runnable and pass it to the <code>SwingUtilities.invokeLater</code> method after the query
has completed.
