==========
Objective: Explain the purpose of keys: primary, natural and candidate
==========

[[
Which three characteristics are required for a primary key in a relational DB table?  (Choose three)
]]
*1: The key must be unique
2: The key must be numeric
*3: The key must never change
*4: The key must never be <code>null</code>
5: The key must be stored in a single field
6: The key must have meaning in the real-world


[[
Your team is building a database that stores weather data for the state of New York.
There are four types of measurements: Temperature, Wind speed, Rain fall and Barometric pressure.
There are hundreds of stations taking these measurements.

Given these requirements, which three data elements should be used to form a unique key?
]]
1: The value of the measurement
2: The units of the measurement
*3: The name the weather station
*4: The timestamp of the measurement
*5: The name of the type of measurement


==========
Objective: Explain a foreign key and how table are related
==========

[[
Which is true about a foreign key?
]]
1:  A foreign key may be any field
2:  A foreign key must be a single field
3:  A foreign key is the same as a primary key
*4: A foreign key is the primary key of another table

[[THOUGHT:
Test constaint violation.  given two tables with FK and insert stmt options; which one fails
]]

[[NOT READY FOR PRIME TIME
You have two DB tables that hold user and role information in your application:

<pre><strong>User</strong>
username: string
email: string</pre>

and:

<pre><strong>UserRole</strong>
username: string (FK)
rolename: string</pre>

A user may have any number of distinct roles; each role has a separate record
in the <code>UserRole</code> table.

Which statement <strong>must</strong> be true to ensure that you can
build a query joining these two tables?
]]
1: The <code>User.username</code> field is a natural key on the <code>User</code> table
1: The <code>UserRole.username</code> field is a foreign key into the <code>User</code> table
1: The <code>username</code> and <code>rolename</code> fields form a primary key on the The <code>UserRole</code> table
1: 
1: 


==========
Objective: Explain the purpose of a trigger
==========

[[
Your project requires the need for simple auditing mechanism that keeps track
of the timestamp and DB user whenever a <code>customer</code> record is changed.

Which SQL tool could you use to automate this auditing requirement?
]]
1: a view on the <code>customer</code> table
2: an index on the <code>customer</code> table
*3: a trigger on the <code>customer</code> table
4: a partition on the <code>customer</code> table
5: a foreign key on the <code>customer</code> table
