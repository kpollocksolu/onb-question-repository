(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Ryan Stacy
(Course Site): Lynda
(Course Name): Building Web Services with Java EE
(Course URL): http://www.lynda.com/Developer-Programming-Languages-tutorials/Building-Web-Services-Java-EE/149837-2.html
(Discipline): Java
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 1
(Random answers): 1
(Question): What are the benefits of using a Web Service instead of a more direct approach?  (Select all that apply)
(A): It provides an interface that can silo changes to functions, databases, etc.
(B): There will be less code
(C): Servers are always faster than client machines
(D): They provide compatibility between multiple languages
(Correct): A, D
(Points): 1
(CF): There is no guarantee that there will be less code or that a server is faster
(WF): There is no guarantee that there will be less code or that a server is faster
(STARTIGNORE)
(Hint): 
(Subject): Java Web Services	
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What is the "Black Box" method? 
(A): Encapsulating processes so that a user can interact with a program with as little information as possible
(B): Making a web service inaccessible to users
(C): Making all class variables private
(D): Requiring a password for all services
(Correct): A
(Points): 1
(CF): The Black Box method is not about locking the user out of a service by any means.  Rather, it lets a user interact with a service without having to know exactly how it works.
(WF): The Black Box method is not about locking the user out of a service by any means.  Rather, it lets a user interact with a service without having to know exactly how it works.
(STARTIGNORE)
(Hint):
(Subject): Java Web Services
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 1
(Random answers): 1
(Question): Which 2 things does your code need to be a Web Service? (Select all that apply)
(A): @WebService on class declaration
(B): A public constructor that takes no inputs
(C): @RESTfulService on class declaration
(D): Special comments
(Correct): A, B
(Points): 1
(CF): There are no special comments for Web Services in Java EE, nor a RESTfulService annotation
(WF): There are no special comments for Web Services in Java EE, nor a RESTfulService annotation
(STARTIGNORE)
(Hint): 
(Subject): Java Web Services
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): WSDL is a HTTP based language.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): It is an XML based language
(WF): It is an XML based language
(STARTIGNORE)
(Hint):
(Subject): Java Web Services
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 1
(Random answers): 1
(Question): Which of the following are required in WSDL? (Select all that apply)
(A): Data type of inputs
(B): Data type of output 
(C): Method length 
(D): Version of Java
(Correct): A, B
(Points): 1
(CF): Method length and version of Java are not required.
(WF): Method length and version of Java are not required.
(STARTIGNORE)
(Hint):
(Subject): Java Web Services
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): RPC is better than SOAP for remote communication.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): RPC is better for computers within a small network
(WF): RPC is better for computers within a small network
(STARTIGNORE)
(Hint): 
(Subject): Java Web Services
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What is a difference between SOAP 1.1 and SOAP 1.2?
(A): Java Support
(B): TCP/IP and MQ support
(C): HTTP support
(D): RPC support
(Correct): B
(Points): 1
(CF): SOAP 1.2 adds TCP/IP and MQ support
(WF): SOAP 1.2 adds TCP/IP and MQ support
(STARTIGNORE)
(Hint): 
(Subject): Java Web Services
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 1
(Random answers): 1
(Question): What formats are RESTful messages in? (Select all that apply)
(A): XML
(B): JSON
(C): HTML
(Correct): A, B
(Points): 1
(CF): HTML is not for describing data.
(WF): HTML is not for describing data.
(STARTIGNORE)
(Hint):
(Subject): Java Web Services
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): True or False - You can transmit binary data with SOAP.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): You can enable MTOM with SOAP, and place binary data in XML.
(WF): You can enable MTOM with SOAP, and place binary data in XML.
(STARTIGNORE)
(Hint):
(Subject): Java Web Services
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What does it mean for a request to be Idempotent?
(A): Running a request again with the same parameters should not cause any more change
(B): You are not allowed to make identical requests
(C): You can only make one request at a time
(D): It is another word for a SOAP service
(Correct): A
(Points): 1
(CF): With Idempotency, you should be able to make a request, then make 50 identical requests in a row without additional changes.
(WF): With Idempotency, you should be able to make a request, then make 50 identical requests in a row without additional changes.
(STARTIGNORE)
(Hint): 
(Subject): Java Web Services
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

