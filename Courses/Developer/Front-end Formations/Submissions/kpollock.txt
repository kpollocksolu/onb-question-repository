(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks
29 The Internet & World Wide Web
29 Excel
30 Java Spring Tutorial

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Kyle Pollock
(Course Site): Pluralsight
(Course Name): Practical HTML5
(Course URL): https://app.pluralsight.com/library/courses/practical-html5/table-of-contents
(Discipline):
(ENDIGNORE)

(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): Where is the best place to insert a JS script to ensure the page loads quickly?
(A): Head of the html
(B): Right below opening body tag
(C): Right above closing body tag
(D): After the closing body tag
(E): Directly in the tag of the targeted html element (inline)
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): 
(Applicability): 
(ENDIGNORE)


(Type): truefalse
(Category): 9
(Grade style): 0
(Random answers): 1
(Question): The selector "article > p" will only target p tags that are direct descendents of an article.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): 
(Applicability): 
(ENDIGNORE)


(Type): truefalse
(Category): 9
(Grade style): 0
(Random answers): 1
(Question): CSS3 is plit up into modules, whereas CSS2.1 was not.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): 
(Applicability): 
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Which of these is not a JS canvas method?
(A): fillRect
(B): strokeRect
(C): drawRect
(D): clearRect
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): 
(Applicability): 
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): What does DOM stand for?
(A): Dynamic Object Model
(B): Document Output Mesh
(C): Dynamic Output Model
(D): Document Object Model
(E): Document Object Mesh
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): 
(Applicability): 
(ENDIGNORE)


(Type): truefalse
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): The W3C Document Object Model (DOM) is a platform and language-neutral interface that allows programs and scripts to dynamically access and update the content, structure, and style of a document.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): 
(Applicability): 
(ENDIGNORE)


(Type): truefalse
(Category): 9
(Grade style): 0
(Random answers): 1
(Question): In CSS3, a linear gradient is horizontal (left -> right) by default.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): 
(Applicability): 
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is the correct JS method to get a specific element by ID from the DOM?
(A): getElementById();
(B): document.get("ID");
(C): document.getElementById("ID");
(D): document.getId("ID);
(E): document("ID").getElement();
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): 
(Applicability): 
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): SVG images lose quality and get grainy as you zoom further in.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): 
(Applicability): 
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Web application cookies are secure.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): 
(Applicability): 
(ENDIGNORE)
