(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Ryan Stacy
(Course Site): Lynda
(Course Name): Javascript Essential Training	
(Course URL): http://www.lynda.com/JavaScript-tutorials/JavaScript-Essential-Training/81266-2.html
(Discipline): JavaScript
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): In which of the following JavaScript statements does the white space matter? 
(A): alert    ("Hello World!");
(B): alert("Hello World!"    );
(C): alert("Hello World!")     ;
(D): alert("Hello                 World!");
(Correct): D
(Points): 1
(CF): The white space only matters when it is inside the string.  That is a string literal that JavaScript will display as is.
(WF): The white space only matters when it is inside the string.  That is a string literal that JavaScript will display as is.
(STARTIGNORE)
(Hint): 
(Subject): JavaScript
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Which of the following is not a valid JavaScript variable name? 
(A): var 2cool4school;
(B): var two_words;
(C): var $_;
(D): var COUNT;
(Correct): A
(Points): 1
(CF): JavaScript variables can contain numbers, letters, the underscore, and the dollar sign, but cannot START with a number.
(WF): JavaScript variables can contain numbers, letters, the underscore, and the dollar sign, but cannot START with a number.
(STARTIGNORE)
(Hint):
(Subject): JavaScript
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What will the following statement in JavaScript return: "hello there".indexOf("there");? 
(A): 5
(B): 6
(C): undefined
(D): NaN
(Correct): B
(Points): 1
(CF): A String literal can still have methods called on it in JavaScript.  The indexOf() method returns the position of the first occurrence of a specified value in a string, returning -1 if not found.
(WF): A String literal can still have methods called on it in JavaScript.  The indexOf() method returns the position of the first occurrence of a specified value in a string, returning -1 if not found.
(STARTIGNORE)
(Hint): 
(Subject): JavaScript
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): When comparing Strings with > or < in JavaScript, the case of the letters can change the outcome.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): "Cba" > "abc" will return false.  "cba" > "abc" will return true!
(WF): "Cba" > "abc" will return false.  "cba" > "aad" will return true!
(STARTIGNORE)
(Hint):
(Subject): JavaScript
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Which statement would create a Date object containing June 10th, 1995?
(A): var date = new Date(1995, 5, 10);
(B): var date = new Date(1995, 6, 9);
(C): var date = new Date(1995, 5, 9);
(D): var date = new Date(1995, 6, 10);
(Correct): A
(Points): 1
(CF): The month parameter is base 0, the day parameter is not!
(WF): The month parameter is base 0, the day parameter is not!
(STARTIGNORE)
(Hint):
(Subject): JavaScript
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): JavaScript events won't fire unless you specifically tell them to.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): JavaScript events happen all the time!  Even mouse movement is an event.
(WF): JavaScript events happen all the time!  Even mouse movement is an event.
(STARTIGNORE)
(Hint): 
(Subject): JavaScript
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Which of the following is not a way to add an event listener?
(A): <button onclick="alert('Click!');"> </button>
(B): myElement.onclick = function() {};
(C): document.addEventListener('click', myFunction, false);
(D): onclick.addNewHandlerMethod(myMethod);
(Correct): D
(Points): 1
(CF): onclick does not have a function addNewHandlerMethod! 
(WF): onclick does not have a function addNewHandlerMethod! 
(STARTIGNORE)
(Hint): 
(Subject): JavaScript
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): How will a browser handle multiple window.onload methods in multiple JavaScript files?
(A): The last one loaded will be the one called
(B): They will all be compiled into one method
(C): An error will print to the console and none of the methods will be called
(D): They will be called one after another
(Correct): A
(Points): 1
(CF): The window.onload will simply be replaced every time it is assigned to.  The last JavaScript file to assign to it will be the one that gets called.
(WF): The window.onload will simply be replaced every time it is assigned to.  The last JavaScript file to assign to it will be the one that gets called.
(STARTIGNORE)
(Hint):
(Subject): JavaScript
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): The following code will call myFunction every 5 seconds: setTimeout(myFunction, 5000);
(A): True
(B): False
(Correct): B
(Points): 1
(CF): setTimeout will only call the method ONCE! setInterval takes the same arguments, but repeats the call at the assigned interval.
(WF): setTimeout will only call the method ONCE! setInterval takes the same arguments, but repeats the call at the assigned interval.
(STARTIGNORE)
(Hint):
(Subject): JavaScript
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is the main purpose of minification of JavaScript files?
(A): To reduce file size
(B): To obfuscate your code
(C): To compile your code
(D): To make code more readable
(Correct): A
(Points): 1
(CF): While minification may make your code much harder to read, the main purpose is to significantly decrease the size of the file.  This can drastically improve page loading times.
(WF): While minification may make your code much harder to read, the main purpose is to significantly decrease the size of the file.  This can drastically improve page loading times.
(STARTIGNORE)
(Hint): 
(Subject): JavaScript
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

