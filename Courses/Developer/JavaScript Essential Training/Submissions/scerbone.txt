(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Stephen Cerbone
(Course Site): lynda.com
(Course Name): JavaScript Essential Training
(Course URL): http://www.lynda.com/JavaScript-tutorials/JavaScript-Essential-Training/81266-2.html
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Where should JavaScript code be located in order for the page to render before it is parsed, while still following to best practices?
(A): In the <head> tag.
(B): In the <meta> tag.
(C): At the beginning of the <body> tag.
(D): At the end of the <body> tag.
(E): At the end of the <html> tag.
(Correct): D
(Points): 1
(CF): JS code should be at the end of the <body> tag in order for the page to load prior to any code, while still following best practices.
(WF): JS code should be at the end of the <body> tag in order for the page to load prior to any code, while still following best practices.
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What value is returned by the following code? return (5 == 6) ? false : true;
(A): true 
(B): false
(C): NaN
(D): Compile Error
(E): Syntax Error
(Correct): A
(Points): 1
(CF): The condition is checked (5 == 6) and the result is false, this causes the false condition to run returning true.
(WF): Although the condition (5 == 6) returns false, the right side of the colon is the false condition causing the statement to return true.
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): JavaScript is a strongly typed language.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): JavaScript is a weakly typed language. This means that variables do not have a single possible type rather they can change dynamically.
(WF): JavaScript is a weakly typed language. This means that variables do not have a single possible type rather they can change dynamically.
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is the function of "continue" in JavaScript?
(A): To break out of the current function and then continue to run the rest of the code.
(B): To stop the current iteration and progress to the next function.
(C): To stop the current iteration and recheck the condition in the current function.
(D): To stop the current iteration and reset the function.
(E): To pause the code for a brief period of time before resuming.
(Correct): C
(Points): 1
(CF): In JavaScript "continue" stops the current iteration and rechecks the condition of the current function.
(WF): In JavaScript "continue" stops the current iteration and rechecks the condition of the current function.
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): In JavaScript a semicolon is required at the end of every statement.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): In JavaScript a semicolon is NOT required at the end of every statement but it is a good practice to avoid issues down the road.
(WF): In JavaScript a semicolon is NOT required at the end of every statement but it is a good practice to avoid issues down the road.
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): How do you declare a class in JavaScript?
(A): class myClass(){...}
(B): public class myClass(){...}
(C): class myClass() extends Classes {...}
(D): class myClass = Class(){...};
(E): There are no classes in JavaScript.
(Correct): E
(Points): 1
(CF): There are no classes in JavaScript.
(WF): There are no classes in JavaScript.
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What type of variable will the following code return? document.getElementsByTagName("p");
(A): String
(B): Integer
(C): Element
(D): Elements
(E): Array
(Correct): E
(Points): 1
(CF): The code will return an array of elements with the tag "p".
(WF): The code will return an array of elements with the tag "p".
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): If the class of an element is changed from JavaScript during runtime when will the changes be seen visually on the page?
(A): Immediately.
(B): Once the web page is refreshed.
(C): When the AJAX thread refreshes.
(D): When the HTML code is updated manually.
(E): The class of an element cannot be changed with JavaScript during runtime.
(Correct): A
(Points): 1
(CF): The class of an element can be changed and seen visually in real-time from JavaScript.
(WF): The class of an element can be changed and seen visually in real-time from JavaScript.
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): If you do not use "use strict"; when writing JavaScript, most browsers will not be able to parse the code.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): The "use strict"; statement is not necessary for most browsers to parse the code.
(WF): The "use strict"; statement is not necessary for most browsers to parse the code.
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): AJAX allows JavaScript to send user input to a server in the background and update the web page without having to reload the page itself.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): AJAX allows JavaScript to send user input to a server in the background and update the web page without having to reload the page itself.
(WF): AJAX allows JavaScript to send user input to a server in the background and update the web page without having to reload the page itself.
(STARTIGNORE)
(Hint):
(Subject): JavaScript Essential Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)