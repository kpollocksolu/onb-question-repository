(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Eclipse IDE

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Joseph Duda
(Course Site): lynda.com
(Course Name): Up and Running with Eclipse
(Course URL): http://www.lynda.com/Eclipse-tutorials/Introducing-Eclipse-debugger/111243/118397-4.html
(Discipline): Technical
(ENDIGNORE)

(Type): truefalse
(Category): 23
(Grade style): 0
(Random answers): 1
(Question): You can add special comment commands such as TODO and FIXME in Eclipse.
(A): True
(B): False 
(Correct): A
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):23
(Grade style): 0
(Random answers): 1
(Question): What does classpath mean?
(A): The directory where your project is stored.
(B): A group of files needed to run a particular piece of code.
(C): The path where you should store your class files.
(D): None of the above.
(Correct): B
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course 
(ENDIGNORE)


(Type): multiplechoice
(Category):23
(Grade style): 0
(Random answers): 1
(Question): Code completion refers to which of the following?
(A): Formatting your code in the way you feel most comfortable with.
(B): Writing proper comments throughout your code so it is completely understood.
(C): Using Eclipse features to complete a project on time.
(D): Using shortcuts that allow Eclipse to fill in or create code for you.
(Correct): D
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course 
(ENDIGNORE)


(Type): multiplechoice
(Category):23
(Grade style): 0
(Random answers): 1
(Question): What do "step over" and "step into" refer to?
(A): Moving blocks of code around to make it more organized.
(B): Executing code line by line in the debugger to try to fix an error.
(C): Switching to a different perspective in Eclipse.
(D): Creating a new project using a plugin that was just installed.
(Correct): B
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course 
(ENDIGNORE)