(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter):
(Course Site):
(Course Name):
(Course URL):
(Discipline):
(ENDIGNORE)

(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): Which term best describes the process of integrating new changes from the trunk/main branch into a feature branch?
(A): Merging
(B): Backward Integration
(C): Forward Integration
(D): Branching
(E): Branch Fusion
(Correct): C
(Points): 1
(CF): Correct!
(WF): Merging is also correct but it isn't the most accurate description of this operation.
(STARTIGNORE)
(Hint): 
(Subject): Version Control
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 1
(Random answers): 1
(Question): Which key version control concepts are accurately tied to their definition?
(A): Add - Insert new files from the working set into the repository
(B): Tag - A numerical identifier for the state of the repository
(C): Check-in - Copy changes from the working set into the repository
(D): Working Set - A set of files in the remote repository that can be copied
(E): Repository - A Local copy of files under version control
(Correct): A,C
(Points): 1
(CF): Correct!
(WF): See Part 1 - Understanding version control concepts
(STARTIGNORE)
(Hint):
(Subject): Version Control
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 1
(Random answers): 1
(Question): Some common Git commands include..
(A): git init
(B): git log
(C): git status
(D): git undo
(E): git build
(Correct): A,B,C
(Points): 1
(CF): Correct!
(WF): See Part 1 - Demo one: Getting started
(STARTIGNORE)
(Hint):
(Subject): Version Control
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): I just made a bunch of changes to a file named 'myprog.c', how can I undo these changes and restore the file to its current state in the repository?
(A): git revert myprog.c
(B): git undo
(C): git checkout myprog.c
(D): git diff myprog.c
(Correct): C
(Points): 1
(CF): Correct!
(WF): See Part 1 - Demo two: Handling the "oops"
(STARTIGNORE)
(Hint):
(Subject): Version Control
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): When getting started with Git you can use the command 'git config --global email "youremail@example.com"' to set your email identification with Git.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Correct!
(WF): The correct command is 'git config --global user.email "youremail@example.com"'
(STARTIGNORE)
(Hint):
(Subject): Version Control
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): When viewing log messages in git (using 'git log'), you can page through the output by pressing the spacebar.
(A): True
(B): False 
(Correct): A
(Points): 1
(CF): Correct!
(WF): See Part 1 - Demo one: Getting started
(STARTIGNORE)
(Hint):
(Subject): Version Control
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 1
(Random answers): 1
(Question): Why is it beneficial to use version control software?
(A): Automatic backups give you peace of mind.
(B): A change-by-change log of your work gives you a history of your progress.
(C): Branching and forking give you easy to create 'sandboxes' for fearless experimentation
(D): Short and long term undo options for restoring previous versions of your work.
(Correct): A,B,C,D
(Points): 1
(CF): Correct!
(WF): See Part 1 - Understanding version control concepts
(STARTIGNORE)
(Hint):
(Subject): Version Control
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): Subversion and perforce are centralized version control systems.
(A): True
(B): False 
(Correct): A
(Points): 1
(CF): Correct!
(WF): See Part 2 - The history of version control
(STARTIGNORE)
(Hint):
(Subject): Version Control
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): What describes the sequence of events in continuous builds?
(A): Code is checked in directly to the repository.
(B): Code is checked in, verified by the system through a build and/or testing, then committed to the repository.
(C): Code is checked in, verified by an admin, then committed to the repository.
(D): Code is never checked in, it must be sent via email to and admin.
(Correct): B
(Points): 1
(CF): Correct!
(WF): See Part 3 - Exploring workflow integration and continuous builds
(STARTIGNORE)
(Hint):
(Subject): Version Control
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 16
(Grade style): 0
(Random answers): 1
(Question): Distributed version control systems lock files while they are in use to prevent conflicts.
(A): True
(B): False 
(Correct): B
(Points): 1
(CF): Correct!
(WF): See Part 3 - Getting files in and out of a repo
(STARTIGNORE)
(Hint):
(Subject): Version Control
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)