(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
26 Experience Design Patterns in Java

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Kamalesh Patil
(Course Site): udemy.com
(Course Name): Complete Java SE 8 Developer Bootcamp
(Course URL): https://www.udemy.com/learn-java-se-8-and-prepare-for-the-java-associate-exam/learn/v4/overview
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 24
(Grade style): 0
(Random answers): 0
(Question): Which of the following types are supported in the switch statement?
(A): String
(B): Enumerated (enum) types
(C): char
(D): All of the above
(Correct): D
(Points): 1
(CF): Starting with Java SE 7 the switch statement works with String type in addition to char and enum types.
(WF): Starting with Java SE 7 the switch statement works with String type in addition to char and enum types.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 24
(Grade style): 0
(Random answers): 1
(Question): Inheritance is described using a _________ relationship whereas composition is described using a __________ relationship.
(A): "IS A", "BY A"
(B): "OF A", "IN A"
(C): "HAS A", "IN A"
(D): "IS A", "HAS A"
(Correct): D
(Points): 1
(CF): Inheritance is described using a "IS A" relationship whereas composition is described using a "HAS A" relationship.
(WF): Inheritance is described using a "IS A" relationship whereas composition is described using a "HAS A" relationship.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 24
(Grade style): 0
(Random answers): 0
(Question): True or False: Concrete classes do not inherit static methods of an implemented interface.
(A): TRUE
(B): FALSE
(Correct): A
(Points): 1
(CF): A concrete class does not inherit static methods of an interface implemented by that class.
(WF): A concrete class does not inherit static methods of an interface implemented by that class.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 24
(Grade style): 0
(Random answers): 1
(Question): What is the outcome of compiling and executing the following program?
public class SomeClass {
    public static void main(String[] args) {
        int i = 10;
        i%=3;
        System.out.println("Value = " + i);
    }
}
(A): Compilation error for expression 'i%=3'
(B): It will print "Value = 10"
(C): It will print "Value = 1"
(D): It will print "Value = 3"
(Correct): C
(Points): 1
(CF): The expression 'i%=3' is a valid expression, a shortform for 'i = i%3'.  And the result of the operation '10 % 3' is 1.
(WF): The expression 'i%=3' is a valid expression, a shortform for 'i = i%3'.  And the result of the operation '10 % 3' is 1.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 24
(Grade style): 0
(Random answers): 0
(Question): True or False: The Adder interface, as shown below, is a functional interface.
interface Adder {
	default int add(int a, int b) {
		return a + b;
	}
}
(A): TRUE
(B): FALSE
(Correct): B
(Points): 1
(CF): A functional interface is an interface with a single abstract method.  The above interface does not have an abstract method.
(WF): A functional interface is an interface with a single abstract method.  The above interface does not have an abstract method.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 24
(Grade style): 0
(Random answers): 0
(Question): What is the outcome of compiling and executing the following program?
public class SomeClass {
    public static void main(String[] args) {
        String[][] grades = { { "A+", "A" }, { "B", "C" }, { "D", "E" } };
        for (int i = 0; i < grades.length; i++) {
            for (int j = 0; j < grades.length; j++) {
                System.out.print(grades[i][j] + " ");
            }
        }
    }
}
(A): The class will not compile.
(B): The class will compile but will not run.
(C): The class will compile and run and print - A+ A B C D E
(D): None of the above
(Correct): D
(Points): 1
(CF): The class will compile and run but will throw java.lang.ArrayIndexOutOfBoundsException.  That is because the grades array has 3 "rows" and 2 "columns" but the program iterates over columns upto "row count" number of times = 3.
(WF): The class will compile and run but will throw java.lang.ArrayIndexOutOfBoundsException.  That is because the grades array has 3 "rows" and 2 "columns" but the program iterates over columns upto "row count" number of times = 3.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 24
(Grade style): 0
(Random answers): 1
(Question): As a general rule, which of the following methods must be overridden when the 'equals()' method is overridden?
(A): toString()
(B): serialize()
(C): compareTo()
(D): hashCode()
(Correct): D
(Points): 1
(CF): As a general rule, the 'hashCode()' method must be overridden when 'equals()' method is overridden.
(WF): As a general rule, the 'hashCode()' method must be overridden when 'equals()' method is overridden.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 24
(Grade style): 0
(Random answers): 1
(Question): Which of the following classes were introduced as part of Java SE 8 Date and Time API?  Please select two.
(A): LocalCalendar
(B): Time
(C): Instant
(D): LocalDate
(Correct): C,D
(Points): 1
(CF):
(WF):
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 24
(Grade style): 0
(Random answers): 1
(Question): Which of the following statements about Java Collections are true?  Please select two.
(A): A Set allows to get and set elements using an index.
(B): A List allows duplicate elements.
(C): A Set allows duplicate elements.
(D): A Map maintains order of elements as they are added to the map ("insertion order").
(E): A List maintains order of elements as they are added to the list ("insertion order").
(Correct): B,E
(Points): 1
(CF): A List collection maintains elements in insertion order and allows duplicate elements.
(WF): A List collection maintains elements in insertion order and allows duplicate elements.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 24
(Grade style): 0
(Random answers): 1
(Question): What will be the output of following program?
public class ExceptionHandlingExample {
    public static void main(String[] args) {
        try {
            try {
                throw new Throwable();
            } catch (Exception e) {
                System.out.print("ERROR-1,");
            } finally {
                System.out.print("FINALLY-1,");
            }
        } catch (Throwable t) {
            System.out.print("ERROR-2,");
        } finally {
            System.out.print("FINALLY-2");
        }
    }
}
(A): FINALLY-1,ERROR-2,FINALLY-2
(B): ERROR-1,FINALLY-1,FINALLY-2
(C): ERROR-1,FINALLY-1,ERROR-2,FINALLY-2
(D): ERROR-1,FINALLY-2
(E): ERROR-2,FINALLY-2
(Correct): A
(Points): 1
(CF): The "catch(Exception)" clause doesn't handle Throwable from inner try block; and both finally blocks are executed.
(WF): The "catch(Exception)" clause doesn't handle Throwable from inner try block; and both finally blocks are executed.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
