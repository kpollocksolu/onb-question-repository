(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Stephen Cerbone
(Course Site): Lynda
(Course Name): Git Essential Training
(Course URL): http://www.lynda.com/Git-tutorials/Git-Essential-Training/100222-2.html?srchtrk=index:1%0Alinktypeid:2%0Aq:GIT%0Apage:1%0As:relevance%0Asa:true%0Aproducttypeid:2
(Discipline): Technical
(ENDIGNORE)

(Type): truefalse
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): Git commit messages should be phrased in past tense, since the work occurred in the past.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Git commit messages should always be phrased in present tense. Although the work took place in the past, it is easier to read in present tense.
(WF): Git commit messages should always be phrased in present tense. Although the work took place in the past, it is easier to read in present tense.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): Git architecture allows the user to put files in what areas?
(A): Working directory
(B): Staging index
(C): Local repository
(D): Remote repository
(E): Stash
(Correct): A, B, C, D, E
(Points): 1
(CF): Git architecture allows the user to put files in all of these areas.
(WF): Git architecture allows the user to put files in all of these areas.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): How do you compare the files in the staging index to those in the working directory?
(A): git diff --staged
(B): git diff --cached
(C): git diff --local
(D): git diff --global
(E): git diff --compare
(Correct): A, B
(Points): 1
(CF): You can compare the files in the staging index to those in the working directory using "git diff --staged" or "git diff --cached"
(WF): You can compare the files in the staging index to those in the working directory using "git diff --staged" or "git diff --cached"
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): Which of the following commands would rename a file called "testFile.txt" to "sameFile.txt"?
(A): git rename "testFile.txt" "sameFile.txt"
(B): git rename testFile.txt sameFile.txt
(C): git move testFile.txt sameFile.txt
(D): git move "testFile.txt" "sameFile.txt"
(E): git mv testFile.txt sameFile.txt
(Correct): E
(Points): 1
(CF): The following command would rename the file correctly: git mv testFile.txt sameFile.txt
(WF): The following command would rename the file correctly: git mv testFile.txt sameFile.txt
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): truefalse
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): When you add a directory to a .gitignore file, the subdirectories are also ignored.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): When a directory is added to a .gitignore file, the subdirectories are also ignored.
(WF): When a directory is added to a .gitignore file, the subdirectories are also ignored.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): The smallest and most widely accepted way to force git to keep track of an "empty" directory is to use a
(A): readme file.
(B): .gitkeep file.
(C): .git file.
(D): .gitignore file.
(E): a subdirectory.
(Correct): B
(Points): 1
(CF): The smallest and most widely accepted way to force git to keep track of an "empty" directory is to use a .gitkeep file.
(WF): The smallest and most widely accepted way to force git to keep track of an "empty" directory is to use a .gitkeep file.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): A tree-ish object can always be defined with a SHA-1 hash.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): A tree-ish object can always be defined with a SHA-1 hash.
(WF): A tree-ish object can always be defined with a SHA-1 hash.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): What are valid options when displaying a git log?
(A): --graph
(B): --format
(C): --all
(D): --some
(E): --decorate
(Correct): A, B, C, E
(Points): 1
(CF): Valid git log options include: --graph, --format, --all, and --decorate.
(WF): Valid git log options include: --graph, --format, --all, and --decorate.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): Which of the following options ignores ALL spacing?
(A): -b
(B): -s
(C): -w
(D): -d
(E): -l
(Correct): 
(Points): 1
(CF): The -w option ignores all spacing.
(WF): The -w option ignores all spacing.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): It is possible to create a new branch and check it out in one line with the following command: git checkout -b <branch_name>.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): This is a valid command. It creates a new branch and checks it out in one line.
(WF): This is a valid command. It creates a new branch and checks it out in one line.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)