(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
26 Experience Design Patterns in Java

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Tsetsen erdene Ganbaatar
(Course Site): udemy.com
(Course Name): Experience Design Patterns in Java
(Course URL): https://www.udemy.com/experience-design-patterns/learn/v4/overview
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 26
(Grade style): 0
(Random answers): 1
(Question): You have a service that you need global access to. There can also only be one instance of these services at any given time. By which pattern should this object be created?
(A): Singleton
(B): Builder
(C): Factory Method
(D): Prototype
(E): Creator
(Correct): A
(Points): 1
(CF): If you need a single, globally accessible instance of something the Singleton pattern is perfect.
(WF): If you need a single, globally accessible instance of something the Singleton pattern is perfect.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 26
(Grade style): 0
(Random answers): 0
(Question): There is an object structure where each class encapsulates which operations can be performed on that class. The number of object types changes often but the number of legal operations doesn't change often. This ia good place to use the Visitor Pattern.
(A): TRUE
(B): FALSE
(Correct): B
(Points): 1
(CF): If the number of objects changes more frequently than the number of operations, then the Visitor pattern would quickly get overwhelmed as each new object type requires updating every visitor.
(WF): If the number of objects changes more frequently than the number of operations, then the Visitor pattern would quickly get overwhelmed as each new object type requires updating every visitor.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 26
(Grade style): 0
(Random answers): 1
(Question): You are designing an Elevator control system. There are elevators, which need to update every button when they land on a floor, and buttons, which need to update every elevator when they are pressed. Which Design Pattern would be best for this?
(A): Observable (every button observes every Elevator and vice versa)
(B): FlyWeight (every button and elevator have access to a flyweight that aggregates every object and provides public access to those objects)
(C): Mediator (every object observes the mediator and the mediator observes every object)
(D): Chain of Responsibility ( every event traverses the collection of buttons or elevators until one of them handles it)
(E): Visitor (every event causes a visitor to visit each object and determine if that object needs to be aware that that event occurred)
(Correct): C
(Points): 1
(CF): The Mediator is perfect for maintaining and updating state changes across objects within a subsystem. The observable pattern would get overwhelmed here and the other patterns listed may work but would be very complicated and unmaintainable.
(WF): The Mediator is perfect for maintaining and updating state changes across objects within a subsystem. The observable pattern would get overwhelmed here and the other patterns listed may work but would be very complicated and unmaintainable.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 26
(Grade style): 1
(Random answers): 1
(Question): Which Patterns are very useful when implementing Undo operations on a project? (Select all that apply)
(A): Command
(B): Composite
(C): Mediator
(D): Facade
(E): Memento
(Correct): A, E
(Points): 1
(CF): Command objects can have undo logic encapsulated as an undo() method and Mementos can save the state before and operation and reset the state after the operation to rollback the effects of the operation.
(WF): Command objects can have undo logic encapsulated as an undo() method and Mementos can save the state before and operation and reset the state after the operation to rollback the effects of the operation.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 26
(Grade style): 0
(Random answers): 1
(Question): To efficiently share immutable objects with shareable state across subsystems, the _____ Pattern is useful.
(A): Composite
(B): Factory Method
(C): FlyWeight
(D): Visitor
(E): Abstract Factory
(Correct): C
(Points): 1
(CF): FlyWeights allow you to lightly share an instance of an object in multiple places without instantiating them again.
(WF): FlyWeights allow you to lightly share an instance of an object in multiple places without instantiating them again.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 26
(Grade style): 0
(Random answers): 1
(Question): You are designing a system that requires an object to behave differently based on some internal state. This internal state can change during runtime. Which pattern is most applicable.
(A): Strategy
(B): State
(C): Command
(D): Observer
(E): Memento
(Correct): B
(Points): 1
(CF): The State pattern allows objects to behave differently based on some internal state without exposing that state to clients.
(WF): The State pattern allows objects to behave differently based on some internal state without exposing that state to clients.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 26
(Grade style): 0
(Random answers): 1
(Question): There is a very complicated legacy subsystem that you need to use in your project. You need to create a single, higher level interface to use the legacy system. Which pattern would be best to allow your code to use the legacy code.
(A): Adapter
(B): Facade
(C): Mediator
(D): Proxy
(E): Singleton
(Correct): B
(Points): 1
(CF): The Facade can create one high level interface allowing clients to interact with the entire subsystem without knowing anything about how it works.
(WF): The Facade can create one high level interface allowing clients to interact with the entire subsystem without knowing anything about how it works.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 26
(Grade style): 0
(Random answers): 0
(Question): The Mediator pattern allows clients outside of a subsystem an easy way to interact with the subsystem.
(A): TRUE
(B): FALSE
(Correct): B
(Points): 1
(CF): The Mediator Pattern is only concerned with helping a subsystem maintain its own internal state between its objects and does not allow external access.
(WF): The Mediator Pattern is only concerned with helping a subsystem maintain its own internal state between its objects and does not allow external access.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 26
(Grade style): 0
(Random answers): 0
(Question): Exception Handling would be a good place to use the Chain of Responsibility, as we can decouple the invocation (the throwing of the exception) to the handling (when the exception is caught)
(A): TRUE
(B): FALSE
(Correct): A
(Points): 1
(CF): Exception handling is a perfect place to use the Chain of Responsibility. Each block can simply dispatch the exception to the block above it until a catch statement waiting to handle that error is found.
(WF): Exception handling is a perfect place to use the Chain of Responsibility. Each block can simply dispatch the exception to the block above it until a catch statement waiting to handle that error is found.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


