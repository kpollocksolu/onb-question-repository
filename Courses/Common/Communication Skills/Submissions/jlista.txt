

(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
2 Axure
6 Communication
0 Generic
8 HTML
1 Java
3 JIRA
4 OOP
5 SDLC
7 Unix Scripting

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jacob Lista
(Course Site):Udemy
(Course Name): Consulting Skills Series Communication
(Course URL):https://www.udemy.com/consulting-skills-series-communication/?dtcode=NXFwWSz1vMzW
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): How many hours does the average employee waste at meetings each month?
(A): 8
(B): 15
(C): 31
(D): 62
(Correct): C
(Points): 1
(CF): The average employee wastes 31 hours at meetings each month.
(WF): The average employee wastes 31 hours at meetings each month.
(STARTIGNORE)
(Hint):
(Subject):Effective Meetings
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is the best reason for publishing the agenda of a meeting before it is held?
(A): To prevent the attendees from feeling nervous about what will be discussed
(B): To allow attendees to give feedback and suggestions
(C): To make sure your supervisor will approve of the meeting
(D): It is not a good idea to publish the agenda of the meeting
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject):Effective Meetings
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is an executive's most precious resource?
(A): Money
(B): Talent
(C): Time
(D): Technology
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject):Effective Meetings
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)



