(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter):Jennifer Forney
(Course Site):udemy
(Course Name):Communication Skills
(Course URL):https://www.udemy.com/consulting-skills-series-communication/
(Discipline):Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What shouldn't you do when attending someone elses meeting? 
(A): Turn off cell phone
(B): Respect seating patterns
(C): Be a 'Ninja' and show how smart you are
(D): Acknowledge introductions
(E): Respect seating patterns
(Correct): C
(Points): 1
(CF): Don't be a Ninja or Salesman.
(WF): Don't be a Ninja or Salesman.
(STARTIGNORE)
(Hint):
(Subject):Communications Meeting
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):6
(Grade style): 0
(Random answers): 3
(Question): What 3 things should you email prior to a conference call, that make up the 3x3 rule?
(A): Agenda
(B): Cell phone number
(C): Purpose
(D): Issues with other team members
(E): Materials
(Correct): A, C & E
(Points): 1
(CF): The 3x3 of a conference call is to email agenda, materials and purpose prior to the meeting.
(WF): The 3x3 of a conference call is to email agenda, materials and purpose prior to the meeting.
(STARTIGNORE)
(Hint):
(Subject):Communications Meeting
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): true/false
(Category):6
(Grade style): 0
(Random answers): 1
(Question): Non-verbal communication is just as important as verbal communication in a meeting?
(A): True
(B): False
(Correct): A
(Points): 1
(CF): In the 'People' section of training it states that non-verbal communication skills are just as important.
(WF): In the 'People' section of training it states that non-verbal communication skills are just as important.
(STARTIGNORE)
(Hint):
(Subject):Communications Meeting
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): true/false
(Category):6
(Grade style): 0
(Random answers): 1
(Question): As a consultant, you should make sure others know that you are there to replace them because you are technically superior.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): As a consultant, you are technically superior, but are not there to replace anyone. 
(WF): As a consultant, you are technically superior, but are not there to replace anyone. 
(STARTIGNORE)
(Hint):
(Subject):Communications Meeting
(Difficulty): Beginner	
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):6
(Grade style): 0
(Random answers): 1
(Question): When dealing with adversity, what shouldn't you do? 
(A): Punish adversaries
(B): Listen to what's being said
(C): Keep the conversation moving
(D): Separate issues
(E): Answer directly
(Correct): A
(Points): 1
(CF): When dealing with adversity, you should not punish the adversary.
(WF): When dealing with adversity, you should not punish the adversary.
(STARTIGNORE)
(Hint):
(Subject):Communications Meeting
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): True/False
(Category):6
(Grade style): 0
(Random answers): 1
(Question): When dealing with executives, you should be presenting all problems to them so they can find the solutions for you. 
(A): True
(B): False
(Correct): B
(Points): 1
(CF): When dealing with executives, you should not discuss problems without solutions.
(WF): When dealing with executives, you should not discuss problems without solutions.
(STARTIGNORE)
(Hint):
(Subject):Communications Meeting
(Difficulty): Beginner
(Applicability):Course
(ENDIGNORE)


(Type):multiplechoice
(Category):6
(Grade style): 0
(Random answers): 1
(Question): More than what percentage of presentations are considered unbearable? 
(A): 99%
(B): 63%
(C): 45%
(D): 50%
(E): 62%
(Correct): D
(Points): 1
(CF): More than 50% of presentations are considered unbearable.
(WF): More than 50% of presentations are considered unbearable.
(STARTIGNORE)
(Hint):
(Subject):Communications Meeting
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): true/false
(Category):6
(Grade style): 0
(Random answers): 1
(Question): When creating slides, you should use photos instead of clipart. 
(A): True
(B): False
(Correct): A
(Points): 1
(CF): When creating slides, you should use photos instead of clipart.
(WF): When creating slides, you should use photos instead of clipart.
(STARTIGNORE)
(Hint):
(Subject):Communications Meeting
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):6
(Grade style): 0
(Random answers): 1
(Question): What percentage of time do most people spend on email?
(A): 38%
(B): 28%
(C): 55%
(D): 78%
(E): 25%
(Correct):B 
(Points): 1
(CF): Most people spend 28% of their time on email. 
(WF): Most people spend 28% of their time on email. 
(STARTIGNORE)
(Hint):
(Subject):Communications Meeting
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): true/false
(Category):6
(Grade style): 0
(Random answers): 1
(Question): When creating an email, it is now ok to use text speak, as everyone is used to it. 
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Avoid text speak when creating an email. 
(WF): Avoid text speak when creating an email. 
(STARTIGNORE)
(Hint):
(Subject):Communications Meeting
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)