(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Hayden Wagner
(Course Site): Lynda
(Course Name): Agile Project Management
(Course URL): https://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Choose the definition that best fits the term ‘use case’
(A): A value focused description of something a customer will do with the product
(B): The description of a set of interactions between a system and one or more actors
(C): A term for a group of user stories
(D): The rules defining when a delivered iteration can be deployed 
(Correct): B
(Points): 1
(CF): 
(WF): A use case defines a system and the interactions of actors with the system. It should not be confused with a user story, which is a value focused description of something a customer will do with the product. A group of user stories is an epic. 
(STARTIGNORE)
(Hint): Define 'use case', not 'user story'
(Subject): Collecting Requirements
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What is a good example of a success criteria found in an agile project charter?
(A): Perform 25,000 transactions within the first 2 months
(B): Have almost perfect uptime
(C): User feedback should be excellent, and they should return to our service frequently
(D): Our mobile app should have 5000 users and 99% uptime
(Correct): A
(Points): 1
(CF): Success criteria should be simple, specific, and measurable tests to see if the project goal was accomplished. They should not specify the functions of an application (do not specify “mobile app” in success criteria).
(WF): Success criteria should be simple, specific, and measurable tests to see if the project goal was accomplished. They should not specify the functions of an application (do not specify “mobile app” in success criteria).
(STARTIGNORE)
(Hint):
(Subject): Planning an Agile Project
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): How is relative estimating used in an agile project?
(A): To allow the product owner, project manager, and development team to be involved the estimating process
(B): To take a relatively long time and produce specific work time estimates
(C): To relate to the client as much as possible when creating user stories
(D): To allow the development team to quickly estimate the effort required to deliver a user story based on existing information
(E): To commit to an exact time to complete a feature and to communicate that time estimate to a supervisor
(Correct): D
(Points): 1
(CF): Relative estimates allow the development team to create quick estimates based on what they do know and avoid wasting time on specific estimates which may not be correct or worthwhile. 
(WF): Relative estimates allow the development team to create quick estimates based on what they do know and avoid wasting time on specific estimates which may not be correct or worthwhile. 
(STARTIGNORE)
(Hint):
(Subject): Planning an Agile Project
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

