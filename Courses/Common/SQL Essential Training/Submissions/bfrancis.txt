(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essentials Training

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brad Francis
(Course Site): lynda
(Course Name): SQL Essentials Training
(Course URL): http://www.lynda.com/SQL-tutorials/SQL-Essential-Training/139988-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): What does the acronym CRUD stand for?
(A): call, retry, update, deny
(B): create, read, update, delete
(C): create, replace, use, delete
(D): call, read, update, demolish
(Correct): B
(Points): 1
(CF): Crud stands for create, read, update and delete.
(WF): Crud stands for create, read, update and delete.
(STARTIGNORE)
(Hint):
(Subject): SQL Quick Start
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Some examples of a DBMS are oracle, sql developer, and mySQL?
(A): True
(B): False
(Correct): A 
(Points): 1
(CF): These are all examples of database management systems.
(WF): These are all examples of database management systems.
(STARTIGNORE)
(Hint):
(Subject): SQL Quick Start
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): The rows returned from a database have a default order to them?
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Even though it might appear to be ordered the rows returned do NOT have a default ordering to them.
(WF): Even though it might appear to be ordered the rows returned do NOT have a default ordering to them.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Why do some of today's DBMS's not follow a similar standard in certain areas?
(A): They don't feel like it
(B): Its not cool to be a follower
(C): Many of the existing DBMS pre-date when a standard was set
(D): None of the above
(Correct): C
(Points): 1
(CF): Many of these DBMS that people deal with today pre-date the standards that were adopted.
(WF): Many of these DBMS that people deal with today pre-date the standards that were adopted.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): What keyword is used to not retrieve duplicate rows?
(A): UNIQUE
(B): NONDUPLICATE
(C): INDIVIDUAL
(D): DISTINCT
(Correct): D
(Points): 1
(CF): The keyword to get unique rows is DISTINCT.
(WF): The keyword to get unique rows is DISTINCT.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
