(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Laurent Fontanel
(Course Site): Lynda
(Course Name): SQL Essential Training
(Course URL): https://www.lynda.com/SQL-tutorials/SQL-Essential-Training/139988-2.html
(Discipline): Technical
(ENDIGNORE)

(Type): truefalse
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): [code]SELECT EmpId, EmpName FROM Employee[/code] might not return rows in any particular order. 
(A): True
(B): False
(Correct): A
(Points): 1
(CF): SQL doesn't guarantee order by default unless an ORDER BY clause is specified. 
(WF): SQL doesn't guarantee order by default unless an ORDER BY clause is specified. 
(STARTIGNORE)
(Hint):
(Subject): SQL Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): How do you get rid of a [code]VIEW[/code] named [code]TopSalesPeople[/code] in your database?
(A): [code]DELETE VIEW TopSalesPeople[/code]
(B): [code]KILL VIEW TopSalesPeople[/code] 
(C): [code]DROP VIEW TopSalesPeople[/code]
(D): [code]DELETE * FROM TopSalesPeople[/code]
(Correct): C
(Points): 1
(CF): Use the DROP COMMAND. 
(WF): Use the DROP COMMAND. 
(STARTIGNORE)
(Hint): Often, commands that apply to TABLEs also apply to VIEWs.
(Subject): SQL Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): What is the SQL command to change an existing table’s schema by removing a column?
(A): [code]ALTER TABLE "table_name" DROP COLUMN "column_name"[/code]
(B): [code]CHANGE TABLE "table_name" DELETE COLUMN "column_name"[/code]
(C): [code]REMOVE "table_name"."column_name" FROM SCHEMA[/code]
(D): [code]ALTER TABLE "table_name" DELETE COLUMN "column_name"[/code]
(E): [code][/code]
(Correct): A
(Points): 1
(CF): Correct syntax is ALTER TABLE "table_name" DROP COLUMN "column_name";
(WF): Correct syntax is ALTER TABLE "table_name" DROP COLUMN "column_name";
(STARTIGNORE)
(Hint):
(Subject): Changing a Table Schema
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

