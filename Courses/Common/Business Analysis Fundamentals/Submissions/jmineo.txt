(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): 6 6line
For example, the line below will use the 'Generic' category

(Category): 15

Your categories:
2 Axure
6 Communication
0 Generic
8 HTML
1 Java
3 JIRA
4 OOP
5 SDLC
7 Unix Scripting

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): Business analysis This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. 
Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): JAM
(Course Site): www.lynda.com
(Course Name): Business Analysis Fundamentals
(Course URL):http://www.lynda.com/Business-Business-Skills-tutorials/Business-Analysis-Fundamentals/156546-2.html?srchtrk=index:1%0Alinktypeid:2%0Aq:agile%0Apage:1%0As:relevance%0Asa:true%0Aproducttypeid:2
(Discipline):Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 15
(Grade style): 1
(Random answers): 1
(Question): What are the characteristics of business analysis? Select all that apply.
(A): Validates organization objectives
(B): Manages requirements
(C): Helps manage changes to project deliverables
(D): Leads the technical team through development
(E): None of the above
(Correct): A, B, C
(Points): 3
(CF): A business analyst is responsible for all requirement gathering, validating the business objectives,
and managing changes to these objectives and requirements throughout the life of the project.
(WF): A business analyst is responsible for all requirement gathering, validating the business objectives,
and managing changes to these objectives and requirements throughout the life of the project.
Business analysts are not responsible for the technical direction of a development team.
(STARTIGNORE)
(Hint): 
(Subject): Business analysis
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 15
(Grade style): 0
(Random answers): 0
(Question): True or False. Business analysts rely on Curiosity, questioning and listening skills, and patience.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): A business analyst must employ all of these skills to be successful.
(WF): A business analyst must employ all of these skills to be successful.
(STARTIGNORE)
(Hint):
(Subject): Business analysis
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 1
(Random answers): 1
(Question): What are the major steps for a project from a business analysts perspective? Select all that apply.
(A): Project Initiation
(B): Planning and collecting requirements
(C): Validating and Controlling requirements
(D): Implementing the Project
(E): Testing the product
(Correct): A, B, C, D
(Points): 4
(CF): The first four are the major steps for a business analyst.
(WF): Overseeing the testing of the product is a possible role for the business analyst in the last phase of the project, but is not a major step or phase.
(STARTIGNORE)
(Hint): Focus is on Major steps
(Subject): Business analysis
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): What is the term used for those who have a vested interest in the project’s outcome?
(A): Shareholder
(B): Project Manager
(C): Technical Lead
(D): Stakeholder
(E): Upper Management
(Correct): D
(Points): 1
(CF): All the other options are examples of a possible stakeholder (except maybe a shareholder)
(WF): Stakeholders are those who may come from varying disciplines, who have a vested interest in the product’s outcome.
(STARTIGNORE)
(Hint): Think in more generic high level terms
(Subject): Business analysis
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 1
(Random answers): 1
(Question): What are some of the tools that a business analyst may use to communicate project objectives?
(A): Flowcharts
(B): User Stories
(C): Data flow diagrams
(D): Context diagrams
(E): All of the above
(Correct): A, B, D
(Points): 3
(CF): There are other tools and deliverables that may be used but these are the top three mentioned in the course
(WF): Data flow diagrams are something more likely to be used by a technical analyst.
(STARTIGNORE)
(Hint): focus on business
(Subject): Business analysis
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 1
(Random answers): 0
(Question): The business analysis initiation consists of what phases? Select all that apply.
(A): Defining the business opportunity or problem
(B): Defining the business objective
(C): Capturing business information
(D): Understanding stakeholders
(E): None of the above
(Correct): A, B, C, D 
(Points): 4
(CF): All are examples (in order) of the initiation phase.
(WF): All are examples (in order) of the initiation phase.
(STARTIGNORE)
(Subject): Business analysis
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 0
(Question): What are the 4 steps to the requirements pyramid?
(A): Project Need
(B): Project requirements
(C): Specifications and design
(D): Execution
(E): All of the above
(Correct): E
(Points): 1
(CF): All four represent the analysis pyramid, The first two represent the “what” or problem domain. The bottom two represent the “how” or solution domain.
(WF): All four represent the analysis pyramid, The first two represent the “what” or problem domain. The bottom two represent the “how” or solution domain.
(STARTIGNORE)
(Subject): Business analysis
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 0
(Question): All business requirements should be SMART. SMART stands for Simple, Measurable, Achievable, Realistic, and Traceable.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): S is for Specific
(WF): S is for Specific. And while Specific requirements may be simple, they are not always simple.
(STARTIGNORE)
(Subject): Business analysis
(Difficulty): Imtermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 1
(Random answers): 1
(Question): Which of the following are examples of Business Analysis documents that can be used and delivered as part of an analysis plan?
(A): Business Process Activity Diagram
(B): Context Diagram
(C): Stakeholder Map
(D): Stakeholder Context Diagram
(E): Traceability Matrix
(Correct): A, B, C, D, E
(Points): 5
(CF): All are examples of Business Analysis documents
(WF): Others include Business Use Case Model, Organizational Chart, and Requirement Breakdown Structure.
(STARTIGNORE)
(Hint): These were included as part of the course materials
(Subject): Business analysis
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): Collecting requirements consists of which of the following steps:
(A): Building  the requirements plan
(B): Gathering requirements
(C): Interviewing
(D): Brainstorming
(E): All of the above
(Correct): E
(Points): 1
(CF): Also includes Observing, Surveying, producing business rules and requirement traceability, and Building requirement traceability
(WF): Also includes Observing, Surveying, producing business rules and requirement traceability, and Building requirement traceability
(STARTIGNORE)
(Subject): Business analysis
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


