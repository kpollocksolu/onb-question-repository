(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Ryan Stacy
(Course Site): Lynda
(Course Name): Business Analysis Fundamentals
(Course URL): http://www.lynda.com/Business-Business-Skills-tutorials/Business-Analysis-Fundamentals/156546-2.html?srchtrk=index:1%0Alinktypeid:2%0Aq:agile%0Apage:1%0As:relevance%0Asa:true%0Aproducttypeid:2
(Discipline): Business Analysis
(ENDIGNORE)

(Type): multiplechoice
(Category): 15
(Grade style): 1
(Random answers): 1
(Question): What does the business analyst need to manage in a change of product? 
(A): Validating proposed changes
(B): Detect when change is needed
(C): Assist with changing plans
(D): Appoint developers to focus on changed user stories
(Correct): A, B, C
(Points): 1
(CF): 
(WF): Before anything happens with specific stories, change must be deemed valid.
(STARTIGNORE)
(Hint): Change is not always deemed necessary after it has been proposed.
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): Which of the following is NOT a function of the business analyst? 
(A): Validate objectives
(B): Manage proposed changes
(C): Manage the daily scrum
(D): Manage requirements process
(Correct): C
(Points): 1
(CF): Correct.  The daily scrum is managed by the scrum master.
(WF): The Scrum Master is the individual assigned to managing the daily stand ups.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): What makes a business analyst well suited to manage testing? 
(A): They are familiar with the details of the technology
(B): They usually come from testing backgrounds
(C): They have significant knowledge of the people and processes involved in the organization
(D): They work closely with the scrum master
(Correct): C
(Points): 1
(CF): 
(WF): The business analyst doesn't need knowledge of specific technology details or have testing experience.  They are aware of a broader picture.
(STARTIGNORE)
(Hint): 
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): What is NOT a well fitting characteristic of a business analyst?
(A): Curiosity
(B): Observational
(C): Patience
(D): Perfectionist
(Correct): D
(Points): 1
(CF): Correct. In product development, things are likely to go wrong and change.  Rather than keep to the original route an analyst developed, they need to be agile and accept change.
(WF): A business analyst will see many things break and change.  Being a perfectionist may hinder their ability to move passed the issues.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): What is at the core of a good business analyst mindset?
(A): Being able to ask "Why?"
(B): Programmatic knowledge
(C): Budget limitations
(D): Time lines
(Correct): A
(Points): 1
(CF): Correct.  It is up to the analyst to determine why something is good for the business need.
(WF): The core of a business analyst is know when something is or is not needed.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): What is NOT an example of a Key Stakeholder?
(A): Subject-Matter experts
(B): Influential team members
(C): People familiar with company processes
(D): A business analyst
(Correct): D
(Points): 1
(CF): 
(WF): A business analyst is the one to keep the key stakeholders aware and comfortable with the product
(STARTIGNORE)
(Hint): 
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): What is Swivel Technology?
(A): When a computer user gathers information from one system but enters it in another
(B): A flexible software that serves multiple purposes
(C): Any Agile tracking software
(D): When a user is able to stay in the same software for their entire job
(Correct): A
(Points): 1
(CF): 
(WF):
(STARTIGNORE)
(Hint): It isn't a good thing
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 1
(Random answers): 1
(Question): Which of the following are good methods for setting targets?
(A): Be specific
(B): Share starting point
(C): Keep a whiteboard of completed user stories
(D): Ask the stakeholders for an end date
(Correct):  A, B
(Points): 1
(CF): 
(WF): Knowing what is done doesn't set a target.  Having an external force assign an end date is not Agile.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): True or False.  It is never too early to determine solutions to a problem.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Correct. Quality information gathering is essential.
(WF): Having a solution too early can limit project options.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): When gathering requirements, which should be separated?
(A): The how and the what
(B): The when and the how
(C): The what and the who
(D): The when and the where
(Correct): A
(Points): 1
(CF): 
(WF): You need to gather WHAT the stakeholders need and HOW you will execute
(STARTIGNORE)
(Hint): 
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

