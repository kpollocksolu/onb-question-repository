(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jeff Sickelco
(Course Site): www.lynda.com
(Course Name): Business Analysis Fundamentals
(Course URL) : http://www.lynda.com/Business-Business-Skills-tutorials/Business-Analysis-Fundamentals/156546-2.html?srchtrk=index:1%0Alinktypeid:2%0Aq:agile%0Apage:1%0As:relevance%0Asa:true%0Aproducttypeid:2
(Discipline): Professional 
(ENDIGNORE)

(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): What does the SMART acronym stand for?
(A): Standardized, Maintainable, Achievable, Realistic, Testable
(B): Search, Model, Add, Remove, Transition
(C): Scalable, Marketable, Attainable, Retraceable, Timely
(D): Set targets, Make a plan, Achieve milestones, Revisit problems, Test results
(E): Specific, Measurable, Achievable, Realistic, Traceable
(Correct): E
(Points): 1
(CF): The SMART standard for requirements is an acronym for Specific, Measurable, Achievable, Realistic, and Traceable.
(WF): The SMART standard for requirements is an acronym for Specific, Measurable, Achievable, Realistic, and Traceable.
(STARTIGNORE)
(Hint):
(Subject): Business Analysis Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15 
(Grade style): 0
(Random answers): 1
(Question): What is the smallest, topmost level at the tip of the requirements pyramid?
(A): Project Need
(B): Requirements
(C): Specs and Design
(D): Execution
(E): Testing
(Correct): A
(Points): 1
(CF): The top level of the requirements pyramid is Project Need.
(WF): The top level of the requirements pyramid is Project Need.
(STARTIGNORE)
(Hint):
(Subject): Business Analysis Fundamentals
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): Which is of the following is NOT good advice for brainstorming?
(A): Strive for a high volume of ideas
(B): Encourage piggy-backing of ideas
(C): Write all ideas down
(D): Do not set a strict time limit
(E): State objectives beforehand
(Correct): D
(Points): 1
(CF): Brainstorming sessions should be time-boxed, preferably about 20 minutes.
(WF): Brainstorming sessions should be time-boxed, preferably about 20 minutes.
(STARTIGNORE)
(Hint):
(Subject): Business Analysis Fundamentals
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): What is NOT part of the desired BA mindset?
(A): Curiosity about how the business process works today
(B): A belief that inefficiencies exist
(C): An understanding of one's role as facilitator between stakeholders and developers
(D): A desire to act as a bridge between business and technical teams
(E): A tendency to look past procedural details in favor of the big picture
(Correct): E
(Points): 1
(CF): A business analyst should give equal weight to the big picture and small details.
(WF): A business analyst should give equal weight to the big picture and small details.
(STARTIGNORE)
(Hint):
(Subject): Business Analysis Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): When should a business analyst capture lessons learned?
(A): Don't necessarily wait until the end of the project to capture lessons learned; it's best to do this throughout the lifespan of the project
(B): At the end of the project
(C): During requirements validation
(D): During the initiation phase
(E): During stakeholder meetings
(Correct): A
(Points): 1
(CF): Capture lessons learned throughout the entire lifespan of the project.
(WF): Capture lessons learned throughout the entire lifespan of the project.
(STARTIGNORE)
(Hint):
(Subject): Business Analysis Fundamentals
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): What are transition requirements?
(A): They are a type of Functional requirement
(B): They are a type of Non-functional requirement
(C): They are implied requirements
(D): They are SMART requirements
(E): They describe what is needed to confirm acceptance of a project
(Correct): E
(Points): 1
(CF): Transition requirements are those which describe what is needed for successful completion of a project.
(WF): Transition requirements are those which describe what is needed for successful completion of a project.
(STARTIGNORE)
(Hint):
(Subject): Business Analysis Fundamentals
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): When prioritizing requirements, what are helpful categories to use to group requirements of similar importance?
(A): Critical and non-critical
(B): Mandatory, important, and nice-to-have
(C): Implied and explicit
(D): Basic and advanced
(E): Functional and non-functional
(Correct): B
(Points): 1
(CF): Use the categories mandatory, important, and nice-to-have to prioritize requirements.
(WF): Use the categories mandatory, important, and nice-to-have to prioritize requirements.
(STARTIGNORE)
(Hint):
(Subject): Business Analysis Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 1
(Random answers): 1
(Question): Which of the following are indicators that you have collected sufficient requirements?
(A): You have at least three requirements from every major stakeholder
(B): The business team seems excited about the possibilities of the project
(C): You can easily compose an "elevator speech" describing business improvements
(D): Your current verification exercises are resulting in only minor adjustments
(E): Stakeholders agree that you understand their needs
(Correct): B, C, D, E
(Points): 1
(CF): All of these are indicators of a completed requirements gathering process except "you have at least three requirements from every major stakeholder." There is no specific number of requirements that applies for all projects.
(WF): All of these are indicators of a completed requirements gathering process except "you have at least three requirements from every major stakeholder." There is no specific number of requirements that applies for all projects.
(STARTIGNORE)
(Hint):
(Subject): Business Analysis Fundamentals
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): Which of the following is a good technique for gathering requirements?
(A): Observing job processes
(B): Sending surveys
(C): Interviewing
(D): Brainstorming
(E): All of the above
(Correct): E
(Points): 1
(CF): All of these techniques are useful in gathering requirements.
(WF): All of these techniques are useful in gathering requirements.
(STARTIGNORE)
(Hint):
(Subject): Business Analysis Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): What does the acronym KRAC, an approach used for evaluating changes to current job processes, stand for?
(A): Keep, remove, add, change
(B): Keep, retry, adjust, correct
(C): Knowledge, roles, abilities, competence
(D): Know, relate, aspire, care
(E): Keep, release, add, communicate
(Correct): A
(Points): 1
(CF): The KRAC process stands for "keep, remove, add, change."
(WF): The KRAC process stands for "keep, remove, add, change."
(STARTIGNORE)
(Hint):
(Subject): Business Analysis Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)