(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Hayden Wagner
(Course Site): Code School
(Course Name): Discover Dev Tools
(Course URL): https://www.codeschool.com/courses/discover-devtools
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What do the blue and red lines mean in the network waterfall graph?
(A): The blue line shows when the DOM content was loaded, and the red line shows when all page content finished loading
(B): The blue line shows when the DOM content was loaded, and the red line shows when the DOM content should have loaded with peak performance
(C): The blue line shows when content will load on the average desktop internet connection, and the red line shows when the content will load on the average mobile internet connection
(D): The blue line marks when the page was visible to users, and the red line indicates a possible performance issue on the network
(Correct): A
(Points): 1
(CF): The blue line in the waterfall graph shows when the DOM content was loaded, and the red line shows when all content (such as images) finished loading.
(WF): The blue line in the waterfall graph shows when the DOM content was loaded, and the red line shows when all content (such as images) finished loading.
(STARTIGNORE)
(Hint):
(Subject): Network Waterfall Graph
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Using the chrome 'Sources' tab, changes made to source files in the browser will automatically overwrite the locally saved project files.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): You can make changes to source files and view the history of changes made in the browser in the chrome ‘Sources’ tab, but edits made to files in the browser are not transferred to the local source files unless you manually overwrite your local files with a ‘save as’ command from the browser.
(WF): You can make changes to source files and view the history of changes made in the browser in the chrome ‘Sources’ tab, but edits made to files in the browser are not transferred to the local source files unless you manually overwrite your local files with a ‘save as’ command from the browser.
(STARTIGNORE)
(Hint):
(Subject): Sources
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 1
(Random answers): 1
(Question): How can you add an attribute to an HTML element in the chrome ‘Elements’ tab? (choose 2)
(A): Right click the element and click the ‘Add Attribute’ option
(B): Right click the element and click the ‘Edit as HTML’ option to edit the entire HTML element content
(C): Right click the element and choose the ‘Copy’ option then paste the new element in an editor to add your attributes before returning it to the browser
(D): Hold your cursor over an element until an attribute list appears and then make an attribute selection
(Correct): A, B
(Points): 1
(CF): The ‘Add Attribute’ and ‘Edit as HTML’ are the provided options for adding attributes to a DOM element in the chrome ‘Elements’ tab.
(WF): The ‘Add Attribute’ and ‘Edit as HTML’ are the provided options for adding attributes to a DOM element in the chrome ‘Elements’ tab.
(STARTIGNORE)
(Hint):
(Subject): Elements
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
