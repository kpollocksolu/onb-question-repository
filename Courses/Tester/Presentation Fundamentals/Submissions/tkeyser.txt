(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter):
(Course Site):
(Course Name):
(Course URL):
(Discipline):
(ENDIGNORE)

(Type): truefalse
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): When your audience is familiar with your topic, then it's safe to user insider language and make comparisons that are common to the group.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): When your audience is familiar with your topic, then it's safe to user insider language and make comparisons that are common to the group.
(WF): When your audience is familiar with your topic, then it's safe to user insider language and make comparisons that are common to the group.
(STARTIGNORE)
(Hint):
(Subject): Planning your presentation
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): fillinblank
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): When you present to a mixed audience, refer _____ content knowledge audience members.
(A): To only high
(B): To both high and low
(C): To only low
(D): None of the above
(Correct): B
(Points): 1
(CF): When you present to a mixed audience, refer to both high and low content knowledge audience members.
(WF): When you present to a mixed audience, refer to both high and low content knowledge audience members.
(STARTIGNORE)
(Hint):
(Subject): Planning your presentation
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Be sure you stay in sync with everyone in the meeting during the presentation and be prepared juggle their level of knowledge.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Be sure you stay in sync with everyone in the meeting during the presentation and be prepared juggle their level of knowledge.
(WF): Be sure you stay in sync with everyone in the meeting during the presentation and be prepared juggle their level of knowledge.
(STARTIGNORE)
(Hint):
(Subject): Planning your presentation
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): The ideas of trees is to not delve into details.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): The ideas of trees is to delve into details. A forest is high level.
(WF): The ideas of trees is to delve into details. A forest is high level.
(STARTIGNORE)
(Hint):
(Subject): Planning your presentation
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Which is not a pillar of influence?
(A): Ethos 
(B): Logos
(C): Pathos
(D): Legos
(Correct): D
(Points): 1
(CF): The three pillars of influence are Ethos (credibility appeal), Logos (logical appeal), and Pathos (emotional appeal).
(WF): The three pillars of influence are Ethos (credibility appeal), Logos (logical appeal), and Pathos (emotional appeal).
(STARTIGNORE)
(Hint):
(Subject): Desigining your presentation
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Speaking fast is always a bad thing.
(A): True
(B): False
(Correct): B 
(Points): 1
(CF): Speaking fast is not always a bad thing. It communicates energy.
(WF): Speaking fast is not always a bad thing. It communicates energy.
(STARTIGNORE)
(Hint):
(Subject): Designing your presentation
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Every presentation needs an intro with a hook (a "so what").
(A): True
(B): False
(Correct): A 
(Points): 1
(CF): Every presentation needs an intro with a hook (a "so what").
(WF): Every presentation needs an intro with a hook (a "so what").
(STARTIGNORE)
(Hint):
(Subject): Designing your slides
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): The practice run should not include visuals.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): The practice run should include visuals.
(WF): The practice run should include visuals.
(STARTIGNORE)
(Hint):
(Subject): Delivering the presentation
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): Being completely calm and serene may work for some before they present, and may be disastrous for others.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Being completely calm and serene may work for some before they present, and may be disastrous for others.
(WF): Being completely calm and serene may work for some before they present, and may be disastrous for others.
(STARTIGNORE)
(Hint):
(Subject): Delivering the presentation
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 17
(Grade style): 0
(Random answers): 1
(Question): What were the non-work related speaking opportunties mentioned in the training?
(A): Motivational messages, toasts and ceremonal speaches.
(B): Speaking at conferences, speaking to high execs
(C): Speaking at trainings, colleges
(D): Not speaking at all outside of work
(Correct): A
(Points): 1
(CF): Take advantage of non-work related speaking opportunities, such as motivational messages, to your kid's soccer team, a toast at social gatherings, or even ceremonial speeches at a family friend's retirement dinner.
(WF): Take advantage of non-work related speaking opportunities, such as motivational messages, to your kid's soccer team, a toast at social gatherings, or even ceremonial speeches at a family friend's retirement dinner.
(STARTIGNORE)
(Hint):
(Subject): Sharpening your skills
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)